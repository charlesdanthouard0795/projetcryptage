#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PEROQ 50
#define TAILLE 30

void Menu(void);
void Perroq(void);


int main()
{


    Menu();


    return 0;
}


void Menu(void)
{

    int a=0;

    FILE *fp;
    FILE *fs;
    FILE *fc;

    fp= fopen("peroq.def","r");
    fs= fopen("source.txt","r");
    fc= fopen("dest.crt","r");
    if(fp==NULL || fs ==NULL)
        {
            if(fp==NULL)
            {
                printf("fichier de cryptage introuvable\t\t");
            }
            if(fs==NULL)
            {
                printf("fichier source introuvable\n");
            }
        printf("\n");
        printf("\n");
        printf("\t\t\t\t******Menu******\n");
        printf("\n");
        printf("\t\t\tchoisissez ce que vous voulez faire(1/2/3)\n");
        printf("\n");
        printf("1-- entrer le code cryptage \t\t");
        printf("2-- ecrire le fichier source \t\t");
        printf("\n");
        scanf("%d",&a);
        fflush(stdin);

        switch(a)
            {
            case 1:
                {
                    fp = fopen("peroq.def","w+");
                    char strp[PEROQ];

                    printf("entrez votre perroquet\n");
                    fgets(strp,PEROQ-1,stdin);
                    fflush(stdin);

                    fprintf(fp,"%s",strp);
                    printf("votre perroquet prend la forme : %s",strp);
                    fclose(fp);
                    break;
                }
            case 2:
                {
                    fs = fopen("source.txt","w+");
                    char strs[TAILLE];

                    printf("\t\t\t\t\tecrivez votre fichier source\n");
                    fgets(strs,TAILLE-1,stdin);
                    fflush(stdin);

                    fprintf(fs,"%s",strs);
                    printf("votre fichier source prend la forme : %s",strs);
                    fclose(fs);
                    break;
                }
            default:
                {
                    printf("Saisie incomprise; recommencer\n");
                    break;
                }
            }
        }
        else
        {
            fclose(fp);
            fclose(fs);
            fclose(fc);
            do
            {
                printf("\t\t\t\t******Menu******\n");
                printf("\n");
                printf("\t\t\tchoisissez ce que vous voulez faire(1/.../8)\n");
                printf("\n");
                printf("1-- lire le fichier de cryptage\t\t");
                printf("2-- lire le fichier source\t\t");
                printf("3-- crypter le fichier source\t\t");
                printf("4-- lire le fichier crypte\t\t");
                printf("5-- (no)supprimer le fichier source\t\t");
                printf("6-- (no)supprimer le fichier de cryptage\t\t");
                printf("7-- (no)supprimer le fichier crypte .crt\t\t");
                printf("8-- quitter\t\t");
                printf("\n");
                scanf("%d",&a);
                fflush(stdin);

                switch(a)
                {
                case 1:
                    {
                        fp= fopen("peroq.def","r");

                        if(fp ==NULL)
                        {
                            printf("une erreur est survenue mais vous l'avez cherche!");
                            return 0;
                        }

                        printf("voici votre perroquet :\n");
                        char p =fgetc(fp);
                        while(p != EOF)
                        {
                            printf("%c",p);
                            p=fgetc(fp);
                        }
                        fclose(fp);
                        break;
                    }
                case 2:
                    {

                        fs= fopen("source.txt","r");

                        if(fs ==NULL)
                        {
                            printf("une erreur est survenue mais vous l'avez cherche!");
                            return 1;
                        }

                        printf("voici votre fichier source.txt :\n");
                        char s =fgetc(fs);
                        while(s != EOF)
                        {
                            printf("%c",s);
                            s=fgetc(fs);
                        }
                        fclose(fs);
                        break;
                    }
                case 3:
                    {
                        fp= fopen("peroq.def","r");
                        fs= fopen("source.txt","r");
                        fc= fopen("dest.crt","w+");

                        printf("le fichier vas etre crypter par notre equipe d'experts\n");

                        char pe=getc(fp);
                        char c=getc(fs);
                        char res;
                        while(c != EOF)
                        {
                            if(pe==EOF || pe=='\n')
                            {
                                fseek(fp,0,SEEK_SET);
                                pe=getc(fp);
                            }
                            res=c-pe;
                            putc(res,fc);
                            c=getc(fs);
                            pe=getc(fp);

                        }
                        fclose(fc);
                        fclose(fs);
                        fclose(fp);
                        break;

                    }
                case 4:
                    {
                        fc= fopen("dest.crt","r");

                        if(fc ==NULL)
                        {
                            printf("une erreur est survenue mais vous l'avez cherche!");
                            return 0;
                        }

                        printf("voici votre fichier crypte :\n");
                        char c =fgetc(fc);
                        while(c != EOF)
                        {
                            printf("%c",c);
                            c=fgetc(fc);
                        }
                        fclose(fc);
                        break;
                    }
                case 5:
                    {
                        char choix='N';
                        printf("le fichier source.txt vas etre supprimer\n");

                        printf("voulez vous continuer? O/N\n");
                        scanf("%c", choix);
                        fflush(stdin);
                        printf("%c",choix);
                        if(choix == 'O'|| choix=='o')
                            {
                                remove("source.txt");
                            }
                        else
                            {
                                printf("le fichier vas etre concerve.");
                            }

                        break;
                    }
                case 6:
                    {
                        char choix='N';
                        printf("le fichier peroq.def vas etre supprimer\n");
                        printf("voulez vous continuer? O/N\n");
                        scanf("%s", choix);

                        if(choix == 'O'|| choix=='o')
                            {
                                remove("peroq.def");
                            }
                        else
                            {
                                printf("le fichier vas etre concerve.");
                            }

                        break;
                    }
                case 7:
                    {
                       char choix='N';
                        printf("le fichier dest.crt vas etre supprimer\n");
                        printf("voulez vous continuer? O/N\n");
                        scanf("%s", choix);

                        if(choix == 'O'|| choix=='o')
                            {
                                remove("dest.crt");
                            }
                        else
                            {
                                printf("le fichier vas etre concerve.");
                            }

                        break;
                    }
                case 8:
                    {

                        printf("Au revoir!\n");
                        return 0;

                    }
                default:
                    {
                        printf("Saisie incomprise; recommencer\n");
                        break;
                    }
                }
            } while(&a != 6);

            }
return 0;

}




